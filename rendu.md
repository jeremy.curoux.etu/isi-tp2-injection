# Rendu "Injection"

## Binome

Nom, Prénom, email: Curoux , Jérémy , jeremy.curoux.etu@univ-lille.fr
Nom, Prénom, email: Demade , Clément , clement.curoux.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?

Le mécanisme mise en place empeche de mettre plusieur daans la chaine exemple:

- 'toto' sera valide
- 'Or 1=1' renveras ne sera pas valide et renvoie "Veuillez entrer une chaine avec uniquement des lettres et des chiffres"

* Est-il efficace? Pourquoi?

Non, car il est possible de le désactivé avec l'outil de dévelloper web

## Question 2

* Votre commande curl
curl 'http://172.28.100.81:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.81:8080/' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.81:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=cou cou&submit=OK'

cela ajoute cou cou dans la base de donné 

## Question 3

Pour pouvoir insérer du texte et l'ip qu'on veut mettre, il faut mettre faire la commande curl
Pour exploiter cette faille, on envoie une requête pour copier la table qui nous intéressent dans l’autre table 
curl --insert "INSERT INTO chaines (coucou,who) VALUES('" + post["chaine"] + "','" + cherrypy.request.remote.ip + "')" */,who) VALUES('" + post["chaine"] + "','" + cherrypy.request.remote.ip + "')"


## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Pour corrigé la faille on ajoute a self.conn.cursor le paramètre prepared = True ce qui fait que maintenant les requete sont stocké dans un tuple ce qui fait que les chaine contenu ne sont plus consdéré comme des commandes SQL

## Question 5

* Commande curl pour afficher une fenetre de dialog.

curl 'http://172.28.100.81:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.81:8080/' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.81:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=cou cou&submit=OK'

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Pour corrigé la faille XXS on a utilisé la fonction escape() provenant du module HTML.
Cette fonction permet de transformer les caractère <,> et & qui sont utilise pour la construction des balises HTML en chaine qui n'est pas interprété par HTML
